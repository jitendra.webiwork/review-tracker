export const API_ROOT = 'http://localhost:5000/api/';


export const URI = {
    CREATE_USER: 'user',
    LOGIN_USER: 'login',
    GET_COUNTRY: 'country',
    GET_STATES: 'state',
    ADD_NEWBUSINESS: 'businessForm',
    ADD_BRANCHES: 'businessBranch',
    GET_REVIEWS: 'fbReviewRoutes'
}