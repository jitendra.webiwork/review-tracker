const mysql = require('mysql');
const fs = require('fs');
const path = require('path');
const config = require('../config/env');

const basename = path.basename(module.filename);

const connection = mysql.createConnection(config.db_url + '?multipleStatements=true');

module.exports.sync = () => {
  if (config.db_config.client != 'mysql') {
    console.log('This function is designed to run against MySQL only!');
    process.exit(0);
  }
  else {
    Promise.all(getFiles(path.join(__dirname), 'sql').map(file => runSQLScript(config.db_config.database, `${file}`)))
        .then(results => {
        process.exit(0);
      })
      .catch(error => {
        console.log(error);
        process.exit(1);
      });
  }
};

const getFiles = (path, extension = '') => {
  if (!extension.startsWith('.')) { extension = '.' + extension; }
  return fs.readdirSync(path).filter(file => file.toLowerCase().endsWith(extension));
};

// returns the contents of the file as a string.
const getFileData = (folder, filename) => {
  let filePath = path.join(folder, filename);
  console.log(filePath);
  return fs.readFileSync(filePath).toString();
};

// returns the contents of the SQL file as a string.  If string passed doesn't end with .sql it will be appended
const getSQLFileData = (filename) => {
  // let filePath = path.join(__dirname, filename.toLowerCase().endsWith('.sql') ? filename : filename + '.sql');
  // return fs.readFileSync(filePath).toString();
  return getFileData(__dirname, filename.toLowerCase().endsWith('.sql') ? filename : filename + '.sql');
};

// Executes a SQL Script from a file
const runSQLScript = (database, filename) => {
  return new Promise( (resolve, reject) => {
    console.log(`use ${database}; ${getSQLFileData(filename)}`);
    connection.query(`use ${database}; ${getSQLFileData(filename)}`, (err, results) => {
      if (err) {
        console.log(`use ${database}; ${getSQLFileData(filename)}`);
        return reject(err);
      }
      else {
        console.log(results);
        return resolve(results);
      }
    });
  });
};

const getColumns = (schema, table) => {
  let sql = `SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE \
             TABLE_SCHEMA="${schema}" AND TABLE_NAME="${table}" AND COLUMN_DEFAULT IS NULL AND EXTRA <> "auto_increment"
             AND COLUMN_NAME NOT IN ('UpdateDate','UpdatedBy');`;  // HACK for seeding Admin tables

  return new Promise( (resolve, reject) => {
    connection.query(sql, (err, results) => {
      if (err) { return reject(err); }
      return resolve(results.map( results=> results.COLUMN_NAME));
    });
  });
};

const insertData = (schema, table, columns, data) => {
  return new Promise( (resolve, reject) => {
    console.log(`Seeding data into ${schema}.${table}`);
    connection.query(`REPLACE INTO ${schema}.${table} (${columns.join(',')}) VALUES ?;`, [data], (err, results) => {
      if (err) {
        return reject(err);
      }
      else {
        console.log(results);
        return resolve(results);
      }
    });
  });
};

const convertObjectArray = (data) => {
  return data.map(x => Object.keys(x).map( key => {
      return x[key];
  }));
};

module.exports.test = () => {
  getColumns('ims_prod','Sponsor')
  .then( (columns) => {
    console.log(columns);
  })
  .catch( (error) => {
    console.log(error);
  });
};

const bulkInsertFileData = (schema, table, data, fileFolder, fileNameProp, contentColumn) => {
  return new Promise((resolve, reject) => {
    try {
      let newData = data.map(x => {
        x[contentColumn] = getFileData(fileFolder, x[fileNameProp]);
        if (contentColumn !== fileNameProp) {delete x[fileNameProp];}
        return x;
      });
      return resolve(bulkInsert(schema, table, newData));
    }
    catch(error) {
      return reject(error);
    }
  });
};

const bulkInsert = (schema, table, data, useDataKeys = false) => {
  return getColumns(schema, table)
  .then( (columns) => {
    if (useDataKeys === true) {
      columns = Object.keys(data[0]);
    }
    return insertData(schema, table, columns, convertObjectArray(data));
  });
};

const getRefData = (filename) => {
  return new Promise( (resolve, reject) => {
    fs.readFile(path.join(__dirname, './' + filename), 'utf8', (err, data) => {
      if (err) { return reject(err); }
      console.log(data);
      return resolve(JSON.parse(data.toString()));
    });
  });
};

// const readRoleMap = (filename) =>{
//   return new Promise( (resolve, reject) => {
//     csvToJson().fromFile(path.join(__dirname,'./'+ filename))
//      .on("end_parsed", (jsonArrayObj) => {
//       return resolve(jsonArrayObj)
//     })
//     .on('error', (err)=>{
//       reject(err)
//     }) 
//   });
// }

module.exports.seedData = () => {
  getRefData('seed_data.json')
    .then(refData => {
      return Promise.all(Object.keys(refData).map((key) => {
        if (key != "Person") {
          return bulkInsert(config.db_config.database, key, refData[key]);
        } else {
          return bulkInsert(config.db_config.database, key, refData[key], true);
        }
      }));
    })
    .then(() => {
      process.exit(0);                                                                       
    })
    .catch((error) => {
      console.log(error);
      process.exit(1);
    });
  
  // readRoleMap('RoleMap.csv').then(roleMap => {
  //   if(roleMap && roleMap.length>0)
  //    return bulkInsert(config.db_config.database, "RoleMap", roleMap);
  // })
  // .then(()=>{
  //   process.exit(0);
  // })
  // .catch((err) => {
  //   console.log(err);
  //   process.exit(1);
  // }) 
  
};

module.exports.seedStores = () => {
  getRefData('stores.json')
    .then(refData => {
     return bulkInsert(config.db_config.database, 'Store', refData.stores);
  })
  .then( () => {
    process.exit(0);
  })
  .catch( (error) => {
    console.log(error);
    process.exit(1);
  });
};

module.exports.seedAttributes = () => {
  getRefData('attributes.json')
    .then(refData => {
      return bulkInsert(config.db_config.database, 'Attribute', refData.Attribute);
    })
    .then( () => {
      process.exit(0);
    })
    .catch( (error) => {
      console.log(error);
      process.exit(1);
    });
};

module.exports.seedCategoryAttributes = () => {
  getRefData('categoryattributes.json')
    .then(refData => {
      return bulkInsert(config.db_config.database, 'CategoryAttribute', refData.CategoryAttribute);
    })
    .then( () => {
      process.exit(0);
    })
    .catch( (error) => {
      console.log(error);
      process.exit(1);
    });
};

module.exports.seedCategoryAttributeProductMappings = () => {
  getRefData('categoryattributeproductmappings.json')
    .then(refData => {
      return bulkInsert(config.db_config.database, 'CategoryAttributeProductMapping', refData.CategoryAttributeProductMapping);
    })
    .then( () => {
      process.exit(0);
    })
    .catch( (error) => {
      console.log(error);
      process.exit(1);
    });
};

module.exports.seedSponsors = () => {
  getRefData('sponsors.json')
    .then(refData => {
     return bulkInsert(config.db_config.database, 'Sponsor', refData.Sponsor);
  })
  .then( () => {
    process.exit(0);
  })
  .catch( (error) => {
    console.log(error);
    process.exit(1);
  });
};

module.exports.seedParticipants = () => {
  getRefData('participants.json')
    .then(refData => {
     return bulkInsert(config.db_config.database, 'Participant', refData.Participants);
  })
  .then( () => {
    process.exit(0);
  })
  .catch( (error) => {
    console.log(error);
    process.exit(1);
  });
};

module.exports.seedTable = (database, table, key = table) => {
  bulkInsert(database, table, refData[key])
    .then(() => {
      process.exit(0);
    })
    .catch((error) => {
      console.log(error);
      process.exit(1);
    });
};

module.exports.seedEFIStoreList = () => {
   getRefData('efi-store-list.json')
    .then(refData => {
      return refData.reduce((rv, store) => {
        rv.push([store.StoreId, store.StoreNumber || null, store.StoreName, store.StoreAddress, store.StoreCity, store.StoreState, store.StoreZip, store.StorePhone ||null, store.RetailerId || null]);
        return rv;
      }, []);
    })
    .then(mappedObjects => {
      let columns = ["StoreId", "StoreNumber", "StoreName", "StoreAddress", "StoreCity", "StoreState", "StoreZip", "StorePhone", "RetailerId"];
      return insertData(config.db_config.database, 'Store', columns, mappedObjects);
    })
    .then(() => {
      process.exit(0);
    })
    .catch((error) => {
      console.log(error);
      process.exit(1);
    });
};

module.exports.seedRetailers = () => {
  getRefData('retailers.json')
    .then(refData => {
     return bulkInsert(config.db_config.database, 'Retailer', refData);
  })
  .then( () => {
    process.exit(0);
  })
  .catch( (error) => {
    console.log(error);
    process.exit(1);
  });
};

