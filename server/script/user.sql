CREATE TABLE IF NOT EXISTS `User`(
    `userId` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(100) NOT NULL,
    `email` varchar(100) NOT NULL,
    `password` varchar(100) NOT NULL,
    `role` int(2) NOT NULL, 
    `isApproved` tinyint(1) NOT NULL,
    `isProfileUpdated` tinyint(1) NOT NULL,   
    `createDate` datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    `createdBy` varchar(100) NOT NULL,
    `updateDate` datetime ON UPDATE CURRENT_TIMESTAMP NULL,    
    `updatedBy` varchar(100) DEFAULT NULL,
    UNIQUE KEY(`email`),
    PRIMARY KEY(`userId`)
)ENGINE= InnoDB DEFAULT CHARSET=latin1;
