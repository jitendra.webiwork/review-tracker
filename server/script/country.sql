CREATE TABLE IF NOT EXISTS `Country`(
    `id` int(11) NOT NULL,
    `sortname` varchar(100) NOT NULL,
    `name` varchar(100) NOT NULL,
    `phoneCode` varchar(50) NOT NULL,
    PRIMARY KEY(`id`)
)ENGINE= InnoDB DEFAULT CHARSET=latin1;