CREATE TABLE IF NOT EXISTS `State`(
    `id` int(11) NOT NULL,
    `name` varchar(100) NOT NULL,
    `country_id` int(15) NOT NULL,
    PRIMARY KEY(`id`)
)ENGINE= InnoDB DEFAULT CHARSET=latin1;

