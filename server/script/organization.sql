CREATE TABLE IF NOT EXISTS `organization`(
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `userId` int(11) NOT NULL,
    `title` varchar(100) NOT NULL,
    `contact_person` varchar(100) NOT NULL,
    `contact_no` varchar(100) NOT NULL,
    `about` varchar(200) NOT NULL, 
    `website` varchar(100) NOT NULL,
    `address1` varchar(100) NOT NULL,   
    `address2` varchar(100) NOT NULL,
    `country` varchar(100) NOT NULL,
    `state` varchar(100) NOT NULL,
    `city` varchar(100) NOT NULL,    
    `zip` varchar(100) NOT NULL,
    UNIQUE KEY(`id`),
    PRIMARY KEY(`id`)
)ENGINE= InnoDB DEFAULT CHARSET=latin1;
