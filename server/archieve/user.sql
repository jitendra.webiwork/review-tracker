CREATE TABLE IF NOT EXISTS `User`(
    `UserId` int(11) NOT NULL AUTO_INCREMENT,
    `firstName` varchar(100) NOT NULL,
    `lastName` varchar(100) NOT NULL,
    `email` varchar(100) NOT NULL,
    `Password` varchar(100) DEFAULT NULL,
    `isApproved` tinyInt(1) NOT NULL,
    `createDate` datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    `UpdateDate` datetime ON UPDATE CURRENT_TIMESTAMP NULL,
    PRIMARY KEY(`UserId`)
)ENGINE= InnoDB DEFAULT CHARSET=latin1;