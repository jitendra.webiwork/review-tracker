CREATE TABLE IF NOT EXISTS `RoleMap`(
    `RoleMapId` int(11) NOT NULL,
    `Auth_Provider_Role` varchar(100) NOT NULL,
    `Application_Role` varchar(100) NOT NULL,
    `createDate` datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    `UpdateDate` datetime ON UPDATE CURRENT_TIMESTAMP NULL,
    PRIMARY KEY(`RoleMapId`)
)ENGINE= InnoDB DEFAULT CHARSET=latin1;