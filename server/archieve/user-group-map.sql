CREATE TABLE IF NOT EXISTS `UserGroupMap`(
    `UserGroupMapId` int(11) NOT NULL AUTO_INCREMENT,
    `UserId` int(11) NOT NULL,
    `GroupId` int(11) NOT NULL,
    `createDate` datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
    `UpdateDate` datetime ON UPDATE CURRENT_TIMESTAMP NULL,
    UNIQUE KEY (`UserId`, `GroupId`),
    PRIMARY KEY(`UserGroupMapId`)
)ENGINE= InnoDB DEFAULT CHARSET=latin1;